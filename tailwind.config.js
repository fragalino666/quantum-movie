/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'purple': '#C25FFF',
        'gray': '#C3C3C3',
        'darkgray': '#818181'
      },
      fontSize: {
        '4xl+': ["2.5em", {'line-height':"1.2"}]
      },
      screens: {
        'lg+': "1100px"
      }
    },
  },
  plugins: [],
}
