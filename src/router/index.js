import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/pages-list',
      name: 'pages-list',
      component: () => import('../views/PageList.vue'),
      meta: {
        title: 'Quantum Movie - Pages List'
      }
    },
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue'),
      meta: {
        title: 'Quantum Movie'
      }
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: () => import('../views/AboutUs.vue'),
      meta: {
        title: 'Quantum Movie - About Us'
      }
    },
    {
      path: '/referal',
      name: 'referal',
      component: () => import('../views/Referal.vue'),
      meta: {
        title: 'Quantum Movie - Referal'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/login.vue'),
      meta: {
        title: 'Quantum Movie - login'
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('../views/SignUp.vue'),
      meta: {
        title: 'Quantum Movie - Sign Up'
      }
    },
    {
      path: '/recover-password',
      name: 'recover-password',
      component: () => import('../views/RecoverPassword.vue'),
      meta: {
        title: 'Quantum Movie - Recover Password'
      }
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/Profile.vue'),
      meta: {
        title: 'Quantum Movie - Profile'
      }
    },
    {
      path: '/profile-affirmations',
      name: 'profile-affirmations',
      component: () => import('../views/ProfileAffirmations.vue'),
      meta: {
        title: 'Quantum Movie - Profile Affirmations'
      }
    },
    {
      path: '/profile-referal',
      name: 'profile-referal',
      component: () => import('../views/ProfileReferal.vue'),
      meta: {
        title: 'Quantum Movie - Profile Referal'
      }
    },
    {
      path: '/profile-growth',
      name: 'profile-growth',
      component: () => import('../views/ProfileGrowth.vue'),
      meta: {
        title: 'Quantum Movie - Quantum Growth'
      }
    },
    {
      path: '/profile-bank',
      name: 'profile-bank',
      component: () => import('../views/ProfileBank.vue'),
      meta: {
        title: 'Quantum Movie - Deposit Bank Account'
      }
    },


    {
      path: '/temporiraly-premium',
      name: 'temporiraly-premium',
      component: () => import('../views/TemporiralyPremium.vue'),
      meta: {
        title: 'Quantum Movie - Temporiraly Premium'
      }
    },
    {
      path: '/temporiraly-home',
      name: 'temporiraly-home',
      component: () => import('../views/TemporiralyHome.vue'),
      meta: {
        title: 'Quantum Movie - Temporiraly Home'
      }
    },
  ]
});

router.beforeEach((toRoute, fromRoute, next) => {
  window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home';

  next();
})

export default router
